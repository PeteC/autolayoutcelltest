//
//  ALTViewCell.h
//  LayoutTest
//
//  Created by Pete Callaway on 25/09/2013.
//  Copyright (c) 2013 Dative Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALTViewCell : UITableViewCell

+ (NSString*)reuseIdentifier;

- (void)configureWithSentence:(NSString*)sentence;

@end
